#!/usr/bin/env python3

import os
import sys
import urllib
try:
    from urllib2 import urlopen, urlencode, URLError, HTTPError, quote
except:
    from urllib.request import urlopen, URLError, HTTPError, quote
    from urllib.parse import urlencode
import ast
import time
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from project_map import ProjectMap
from utils.utils import reliable_url_open
from options import Options

server = ProjectMap().build_spec().master_host()

url = "http://" + server + "/computer/api/python"
f = urlopen(url)
host_dict = ast.literal_eval(f.read().decode('utf-8'))

def is_excluded(host):
    if ("builder" in host or host == "master" or "simdrm" in host or "Built-In" in host):
        return True

opts = Options()
for a_host in host_dict['computer']:
    offline = a_host['offline']
    if offline:
        print("Skipping system because it is offline: "
              + a_host['displayName'])
        continue
    host = a_host['displayName']
    if is_excluded(host):
        continue
    if (opts.hardware and (opts.hardware != 'builder')
        and (opts.hardware not in host)):
        continue
    f = {'label' : host}
    url = "http://" + server + "/job/public/job/reboot_single/buildWithParameters?" + urlencode(f)
    print("triggering " + url)
    reliable_url_open(url, method="POST")
    time.sleep(1)
